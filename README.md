# 《Java 8 实战》笔记 #

## [《Java8实战》笔记（01）：为什么要关心Java8](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8801%EF%BC%89%EF%BC%9A%E4%B8%BA%E4%BB%80%E4%B9%88%E8%A6%81%E5%85%B3%E5%BF%83Java8)

## [《Java8实战》笔记（02）：通过行为参数传递代码](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8802%EF%BC%89%EF%BC%9A%E9%80%9A%E8%BF%87%E8%A1%8C%E4%B8%BA%E5%8F%82%E6%95%B0%E4%BC%A0%E9%80%92%E4%BB%A3%E7%A0%81)

## [《Java8实战》笔记（03）：Lambda表达式](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8803%EF%BC%89%EF%BC%9ALambda%E8%A1%A8%E8%BE%BE%E5%BC%8F)

## [《Java8实战》笔记（04）：引入流](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8804%EF%BC%89%EF%BC%9A%E5%BC%95%E5%85%A5%E6%B5%81)

## [《Java8实战》笔记（05）：使用流](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8805%EF%BC%89%EF%BC%9A%E4%BD%BF%E7%94%A8%E6%B5%81)

## [《Java8实战》笔记（06）：用流收集数据](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8806%EF%BC%89%EF%BC%9A%E7%94%A8%E6%B5%81%E6%94%B6%E9%9B%86%E6%95%B0%E6%8D%AE)

## [《Java8实战》笔记（07）：并行数据处理与性能](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8807%EF%BC%89%EF%BC%9A%E5%B9%B6%E8%A1%8C%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86%E4%B8%8E%E6%80%A7%E8%83%BD)

## [《Java8实战》笔记（08）：重构、测试和调试](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8808%EF%BC%89%EF%BC%9A%E9%87%8D%E6%9E%84%E3%80%81%E6%B5%8B%E8%AF%95%E5%92%8C%E8%B0%83%E8%AF%95)

## [《Java8实战》笔记（09）：默认方法](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8809%EF%BC%89%EF%BC%9A%E9%BB%98%E8%AE%A4%E6%96%B9%E6%B3%95)

## [《Java8实战》笔记（10）：用Optional取代null](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8810%EF%BC%89%EF%BC%9A%E7%94%A8Optional%E5%8F%96%E4%BB%A3null)

## [《Java8实战》笔记（11）：CompletableFuture-组合式异步编程](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8811%EF%BC%89%EF%BC%9ACompletableFuture-%E7%BB%84%E5%90%88%E5%BC%8F%E5%BC%82%E6%AD%A5%E7%BC%96%E7%A8%8B)

## [《Java8实战》笔记（12）：新的日期和时间API](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8812%EF%BC%89%EF%BC%9A%E6%96%B0%E7%9A%84%E6%97%A5%E6%9C%9F%E5%92%8C%E6%97%B6%E9%97%B4API)

## [《Java8实战》笔记（13）：函数式的思考](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8813%EF%BC%89%EF%BC%9A%E5%87%BD%E6%95%B0%E5%BC%8F%E7%9A%84%E6%80%9D%E8%80%83)

## [《Java8实战》笔记（14）：函数式编程的技巧](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8814%EF%BC%89%EF%BC%9A%E5%87%BD%E6%95%B0%E5%BC%8F%E7%BC%96%E7%A8%8B%E7%9A%84%E6%8A%80%E5%B7%A7)

## [《Java8实战》笔记（15）：面向对象和函数式编程的混合-Java 8和Scala的比较](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8815%EF%BC%89%EF%BC%9A%E9%9D%A2%E5%90%91%E5%AF%B9%E8%B1%A1%E5%92%8C%E5%87%BD%E6%95%B0%E5%BC%8F%E7%BC%96%E7%A8%8B%E7%9A%84%E6%B7%B7%E5%90%88-Java%208%E5%92%8CScala%E7%9A%84%E6%AF%94%E8%BE%83)

## [《Java8实战》笔记（16）：结论以及Java的未来](note/%E3%80%8AJava8%E5%AE%9E%E6%88%98%E3%80%8B%E7%AC%94%E8%AE%B0%EF%BC%8816%EF%BC%89%EF%BC%9A%E7%BB%93%E8%AE%BA%E4%BB%A5%E5%8F%8AJava%E7%9A%84%E6%9C%AA%E6%9D%A5)