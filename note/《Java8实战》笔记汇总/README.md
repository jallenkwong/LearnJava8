```javascript
//oschina
var titles = []
$('.content .header').each(function(i,e){
	var title = $(e).text().trim()
	var startIndex = title.search('《Java8实战》笔记') 
	if(startIndex > 0){
		var href = $(e).attr('href')
		titles.unshift('## [' + title.substr(startIndex) + '](' + href + ')\n\n')
	}
})

var result = ''
titles.forEach(function(value){result += (value)})
console.log(result)
```

```javascript
//csdn
var titles = []
$('.article-item-box h4 a').each(function(i,e){
	var title = $(e).text().trim()
	var startIndex = title.search('《Java8实战》笔记') 
	if(startIndex > 0){
		var href = $(e).attr('href')
		titles.unshift('## [' + title.substr(startIndex) + '](' + href + ')\n\n')
	}
})

var result = ''
titles.forEach(function(value){result += (value)})
console.log(result)
```

```javascript
//gitee
var titles = []
$('.tree-list-item a').each(function(i,e){
	var title = $(e).attr('title')
	var startIndex = title.search('《Java8实战》笔记（') 
	if(startIndex >= 0){
		var href = $(e).attr('href')
		var startIndex2 = href.search('note/') 
		
		titles.push('## [' + title.substr(startIndex) + '](' + href.substr(startIndex2) + ')\n\n')
	}
})
var result = ''
titles.forEach(function(value){result += (value)})
console.log(result)
```
